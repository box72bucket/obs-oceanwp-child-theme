<?php
/**
 * Child theme functions
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 * Text Domain: oceanwp
 *
 * @link http://codex.wordpress.org/Plugin_API
 */

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
function oceanwp_child_enqueue_parent_style() {
	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'OceanWP' );
	$version = $theme->get( 'Version' );
	// Load the stylesheet
	wp_enqueue_style( 'obs-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'oceanwp-style' ), $version );

}

add_action( 'wp_enqueue_scripts', 'oceanwp_child_enqueue_parent_style' );

/**
 * Add the OceanWP Settings metabox in CPTs Product and Category
 *
 * @param array $types previously supported types.
 *
 * @return array now supported types.
 */
function obs_oceanwp_metabox( $types ) {
	$types[] = 'oneboxshop_category';
	$types[] = 'oneboxshop_product';

	return $types;
}

add_filter( 'ocean_main_metaboxes_post_types', 'obs_oceanwp_metabox', 20 );

/**
 * Update the title of the category page to show the correct title
 *
 * @param string $title The original title.
 *
 * @return string The new title.
 */
function obs_update_title( $title ) {
	global $wp;
	if ( isset( $wp->oneboxshop_page ) && $wp->oneboxshop_page instanceof Oneboxshop_Virtual_Page ) {
		$cpt_type = $wp->oneboxshop_page->get_oneboxshop_cpt_type();

		if ( 'category-page' === $cpt_type || 'product-page' === $cpt_type ) {
			return $wp->oneboxshop_page->get_title();
		}
	}

	return $title;
}

add_filter( 'ocean_title', 'obs_update_title' );
